type t;
[@bs.module "aws-sdk"] [@bs.new] external make: unit => t = "S3";

module GetObjectRequest = {
  [@bs.deriving abstract]
  type t = {
    [@bs.as "Bucket"]
    bucket: string,
    [@bs.as "Key"]
    key: string,
  };
  let make = t;
};

module GetObjectResponse = {
  [@bs.deriving abstract]
  type t = {
    [@bs.as "Body"]
    body: Node.Buffer.t,
  };
};

module PutObjectRequest = {
  [@bs.deriving abstract]
  type t = {
    [@bs.as "Body"]
    body: string,
    [@bs.as "Bucket"]
    bucket: string,
    [@bs.as "Key"]
    key: string,
  };
  let make = t;
};

module PutObjectResponse = {
  [@bs.deriving abstract]
  type t = {
    [@bs.as "ETag"]
    etag: string,
  };
};

[@bs.send]
external getObject:
  (t, GetObjectRequest.t, KitchenAsync.jsCallback(GetObjectResponse.t)) =>
  unit =
  "";

[@bs.send]
external putObject:
  (t, PutObjectRequest.t, KitchenAsync.jsCallback(PutObjectResponse.t)) =>
  unit =
  "";
